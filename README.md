# OpenStack SWIFT Library for Rust

This is rust library with partial support of OpenStack Swift Storage API. 

# Getting started

```toml
[dependencies]
openstack-swift-rs = "0.3.0"
```

## Methods

- `login_to_remote_storage`
- `list_container_files`
- `upload_file`
- `delete_file`
- `delete_files`

## Mock feature

Library provides `mock` feature which help to mock some api calls (disabled by default).

Methods:

- `get_login_mock`

Add to your `Cargo.toml`:

```toml
[dev-dependencies]
openstack-swift-rs = { version = "0.3.0", features = ["mock"] }
```


## Run tests

1. Define environment variables:

   1. `SWIFT_AUTH_URL` with base API url. Example: `https://api.selcdn.ru`.
   2. `SWIFT_STORAGE_URL` with base API url. Example: `https://api.selcdn.ru/v1/SEL_54331`.
   3. `SWIFT_STORAGE_USER`
   4. `SWIFT_STORAGE_PASSWORD`
   5. `SWIFT_STORAGE_CONTAINER`

2. Start Web-server on localhost:80, it should respond from `http://localhost` with `HTTP OK 200`.
