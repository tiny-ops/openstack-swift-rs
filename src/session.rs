use std::fmt::{Display, Formatter};

use chrono::Local;

/// Auth token ttl (20 hours)
pub const AUTH_TOKEN_TTL: i64 = 72_000;

pub struct SwiftStorageSession {
    pub auth_token: String,
    pub storage_url: String,
    pub created: i64,
}

impl SwiftStorageSession {
    pub fn is_session_expired(&self) -> bool {
        (Local::now().timestamp() - self.created > AUTH_TOKEN_TTL) ||
            self.auth_token.is_empty()
    }
}

impl Display for SwiftStorageSession {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f, "[SwiftStorageSession] auth_token='********', storage_url='{}', created={}",
            self.storage_url, self.created
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::{DateTime, Local, TimeZone};

    use crate::session::SwiftStorageSession;

    #[test]
    fn return_true_for_active_session() {
        let created: DateTime<Local> = Local::now();

        let session = SwiftStorageSession {
            auth_token: "whatever".to_string(),
            storage_url: "whatever".to_string(),
            created: created.timestamp(),
        };

        assert!(!session.is_session_expired());
    }

    #[test]
    fn return_false_for_expired_session() {
        let expired: DateTime<Local> = Local.ymd(2021, 1, 1)
            .and_hms(13, 12, 23);

        let session = SwiftStorageSession {
            auth_token: "whatever".to_string(),
            storage_url: "whatever".to_string(),
            created: expired.timestamp(),
        };

        assert!(session.is_session_expired());
    }
}
