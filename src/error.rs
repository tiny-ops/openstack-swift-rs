use reqwest::StatusCode;

use crate::error::types::OperationError;

pub mod types {
    use std::io;

    use reqwest::header::ToStrError;
    use thiserror::Error;

    #[derive(Error, Debug)]
    pub enum OperationError {
        #[error("io error")]
        IOError(#[from] io::Error),

        #[error("unable to get string from value")]
        ToStringError(#[from] ToStrError),

        #[error("network error")]
        ConnectionError(#[from] reqwest::Error),

        #[error("resource not found error")]
        ResourceNotFoundError,

        #[error("permissions error")]
        ApiPermissionsError,

        #[error("unsupported api error")]
        UnsupportedApiError,

        #[error("authentication error")]
        Authentication
    }

}

pub fn get_operation_error_by_status_code(status_code: StatusCode) -> OperationError {
    match status_code {
        StatusCode::UNAUTHORIZED => OperationError::Authentication,
        StatusCode::FORBIDDEN => OperationError::ApiPermissionsError,
        StatusCode::NOT_FOUND => OperationError::ResourceNotFoundError,
        _ => OperationError::UnsupportedApiError
    }
}
