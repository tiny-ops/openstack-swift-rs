extern crate log;

pub mod error;

pub mod login;
pub mod session;
pub mod list;
pub mod upload;
pub mod delete;
pub mod url;

#[cfg(feature = "mock")]
pub mod mock;

#[cfg(test)]
mod test_utils;

pub const AUTH_USER_HEADER: &str = "x-auth-user";
pub const AUTH_KEY_HEADER: &str = "x-auth-key";

pub const AUTH_TOKEN_HEADER: &str = "x-auth-token";
pub const STORAGE_URL_HEADER: &str = "x-storage-url";

pub const AUTH_ENDPOINT: &str = "/auth/v1.0";
