use log::{debug, error, info};
use reqwest::blocking::Client;
use reqwest::StatusCode;

use crate::AUTH_TOKEN_HEADER;
use crate::error::get_operation_error_by_status_code;
use crate::error::types::OperationError;
use crate::session::SwiftStorageSession;
use crate::url::normalize_url_slashes;

/// Get file list for container
pub fn list_container_files(client: &Client, session: &SwiftStorageSession,
                            container: &str) -> Result<Vec<String>, OperationError> {
    info!("get files list for container '{}'", container);

    let raw_url = format!("{}/{}", session.storage_url, container);
    let url = normalize_url_slashes(&raw_url);

    match client.get(&url)
        .header(AUTH_TOKEN_HEADER, &session.auth_token)
        .send() {
        Ok(resp) => {
            debug!("response code: {}", resp.status());

            if resp.status() == StatusCode::OK {
                let body = resp.text()?;

                debug!("<response body>");
                debug!("{}", body);
                debug!("</response body>");

                let files = body.split("\n").collect::<Vec<&str>>()
                                            .iter().filter(|v|!v.is_empty())
                                            .map(|v|v.to_string())
                                            .collect::<Vec<String>>();

                Ok(files)

            } else  {
                Err(get_operation_error_by_status_code(resp.status()))
            }
        }
        Err(e) => {
            error!("couldn't get file list from container: {}", e);
            Err(OperationError::ConnectionError(e))
        }
    }
}

#[cfg(test)]
mod tests {
    use log::error;
    use reqwest::blocking::Client;

    use crate::error::types::OperationError;
    use crate::list::list_container_files;
    use crate::login::login_to_remote_storage;
    use crate::test_utils::{get_connection_profile, get_invalid_session, init_logging};

    #[test]
    fn return_container_file_list() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username,
            &connection_profile.password) {
            Ok(session) => {
                match list_container_files(&client, &session, &connection_profile.container) {
                    Ok(files) => {
                        let expected_files = vec![
                          "jvm-micrometer.json".to_string(), "springboot.json".to_string()
                        ];

                        assert_eq!(files, expected_files);
                    },
                    Err(e) => {
                        error!("{:?}", e);
                        panic!("result expected")
                    }
                }
            }
            Err(_) => panic!("valid session expected")
        }
    }

    #[test]
    fn return_error_for_invalid_container_name() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username,
            &connection_profile.password) {
            Ok(session) => {
                match list_container_files(&client, &session, "unknown-container") {
                    Ok(_) => panic!("resource not found error expected"),
                    Err(e) => {
                        match e {
                            OperationError::ResourceNotFoundError => assert!(true),
                            _ => panic!("resource not found error expected")
                        }
                    }
                }
            }
            Err(_) => panic!("valid session expected")
        }
    }

    #[test]
    fn return_auth_error_for_invalid_session() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        let session = get_invalid_session(&connection_profile.storage_url);

        match list_container_files(&client, &session, &connection_profile.container) {
            Ok(_) => panic!("auth error expected"),
            Err(e) => {
                match e {
                    OperationError::Authentication => assert!(true),
                    _ => panic!("auth error expected")
                }
            }
        }
    }
}
