use std::fs::File;
use std::io::Read;

use log::{debug, error, info};
use reqwest::blocking::Client;
use reqwest::StatusCode;

use crate::AUTH_TOKEN_HEADER;
use crate::error::get_operation_error_by_status_code;
use crate::error::types::OperationError;
use crate::session::SwiftStorageSession;
use crate::url::normalize_url_slashes;

pub const CONTENT_LENGTH_HEADER: &str = "content-length";

///=========================================
/// Upload file
///=========================================
///
/// Arguments:
///
/// `session` - user session
///
/// `src_file_path` - source file path
///
/// `remote_file_path` - target file path
///
pub fn upload_file(client: &Client,
                   session: &SwiftStorageSession, src_file_path: &str,
                   remote_file_path: &str) -> Result<(), OperationError> {
    info!("upload file '{}' to remote storage '{}'", src_file_path, remote_file_path);

    let raw_url = format!("{}/{}", session.storage_url, remote_file_path);
    let url = normalize_url_slashes(&raw_url);

    info!("url: '{}'", url);

    let mut file = File::open(src_file_path)?;

    let mut bytes: Vec<u8> = Vec::new();

    let bytes_read = &file.read_to_end(&mut bytes)?;
    debug!("bytes read: {}", bytes_read);

    let content_length = file.metadata()?.len();
    info!("- file size: {}", content_length);

    match client.put(&url)
        .header(AUTH_TOKEN_HEADER, &session.auth_token)
        .header(CONTENT_LENGTH_HEADER, content_length)
        .body(bytes)
        .send() {
        Ok(resp) => {
            debug!("response code: {}", resp.status());

            match resp.status() {
                StatusCode::CREATED => {
                    info!("file has been uploaded to remote storage");
                    Ok(())
                }
                _ => {
                    error!("couldn't upload file");
                    Err(get_operation_error_by_status_code(resp.status()))
                }
            }
        }
        Err(e) => {
            error!("unable to upload file: {}", e);
            Err(OperationError::ConnectionError(e))
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use chrono::{DateTime, Local};
    use httpmock::Method::PUT;
    use httpmock::MockServer;
    use reqwest::blocking::Client;
    use reqwest::StatusCode;
    use tempfile::NamedTempFile;

    use crate::AUTH_TOKEN_HEADER;
    use crate::error::types::OperationError;
    use crate::login::login_to_remote_storage;
    use crate::session::SwiftStorageSession;
    use crate::test_utils::{get_connection_profile, get_invalid_session, get_random_string, init_logging};
    use crate::upload::{CONTENT_LENGTH_HEADER, upload_file};

    #[test]
    fn double_slashes_should_be_removed_from_request_url() {
        init_logging();

        let connection_profile = get_connection_profile();

        let server = MockServer::start();

        let src_file = NamedTempFile::new().unwrap();
        let file_path = format!("{}", src_file.path().display());

        let content = get_random_string();

        fs::write(&file_path, &content).unwrap();

        let target_path = format!("/{}/test_ok", &connection_profile.container);

        let client = Client::new();
        let session = SwiftStorageSession {
            auth_token: get_random_string(),
            storage_url: server.url("").to_string(),
            created: 0
        };

        let expected_target_path = format!("/{}/test_ok", &connection_profile.container);

        let upload_mock = server.mock(|when, then| {
            when.method(PUT)
                .header_exists(AUTH_TOKEN_HEADER)
                .header_exists(CONTENT_LENGTH_HEADER)
                .path(expected_target_path);
            then.status(StatusCode::CREATED.as_u16());
        });

        assert!(
            upload_file(
                &client, &session, &file_path, &target_path
            ).is_ok()
        );

        upload_mock.assert();
    }

    #[test]
    fn return_ok_after_success_upload() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &connection_profile.password) {
            Ok(session) => {

                let src_file = NamedTempFile::new().unwrap();
                let file_path = format!("{}", src_file.path().display());

                let content = get_random_string();

                fs::write(&file_path, &content).unwrap();

                let target_path = format!("{}/test_ok", &connection_profile.container);

                match upload_file(&client, &session, &file_path, &target_path) {
                    Ok(_) => assert!(true),
                    Err(_) => panic!("result expected")
                }

            },
            Err(_) => panic!("session expected")
        }
    }

    #[test]
    fn return_api_perms_error_for_invalid_target_path() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &connection_profile.password) {
            Ok(session) => {

                let src_file = NamedTempFile::new().unwrap();
                let file_path = format!("{}", src_file.path().display());

                fs::write(&file_path, &get_random_string()).unwrap();

                match upload_file(&client, &session, &file_path,
                                  &get_random_string()) {
                    Ok(_) => panic!("ApiPermissionsError error expected"),
                    Err(e) => {
                        match e {
                            OperationError::ApiPermissionsError => assert!(true),
                            _ => panic!("ApiPermissionsError error expected")
                        }
                    }
                }

            },
            Err(_) => panic!("session expected")
        }
    }

    #[test]
    fn return_io_error_if_source_file_was_not_found() {
        init_logging();

        let client = Client::new();

        let now: DateTime<Local> = Local::now();

        let connection_profile = get_connection_profile();

        let session = SwiftStorageSession {
            auth_token: get_random_string(),
            storage_url: connection_profile.storage_url,
            created: now.timestamp(),
        };

        match upload_file(&client, &session,
                          &get_random_string(),
                          &get_random_string()) {
            Ok(_) => panic!("io error expected"),
            Err(e) => {
                match e {
                    OperationError::IOError(_) => assert!(true),
                    _ => panic!("io error expected")
                }
            }
        }
    }

    #[test]
    fn return_auth_error_for_invalid_session() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        let session = get_invalid_session(&connection_profile.storage_url);

        let src_file = NamedTempFile::new().unwrap();

        let file_path = format!("{}", src_file.path().display());

        match upload_file(&client, &session, &file_path,
                          &get_random_string()) {
            Ok(_) => panic!("auth error expected"),
            Err(e) => {
                match e {
                    OperationError::Authentication => assert!(true),
                    _ => panic!("auth error expected")
                }
            }
        }
    }

    #[test]
    fn return_auth_error_if_server_respond_with_unauthorized_error() {
        init_logging();

        let server = MockServer::start();
        let insurance_api_base_url = server.url("");

        let auth_token = &get_random_string();

        let src_file = NamedTempFile::new().unwrap();

        fs::write(src_file.path(), &get_random_string()).unwrap();

        let file_len = src_file.as_file().metadata().unwrap().len();

        let src_file_path = format!("{}", src_file.path().display());

        let remote_file_path = "remote/file";

        let request_file_path = format!("/{}", remote_file_path);

        let upload_mock = server.mock(|when, then| {
            when.method(PUT)
                .path(&request_file_path)
                .header(AUTH_TOKEN_HEADER, auth_token)
                .header(CONTENT_LENGTH_HEADER, file_len.to_string());
            then.status(StatusCode::UNAUTHORIZED.as_u16());
        });

        let session = SwiftStorageSession {
            auth_token: auth_token.to_string(),
            storage_url: insurance_api_base_url.to_string(),
            created: 0,
        };

        let client = Client::new();

        assert!(
            upload_file(
                &client, &session, &src_file_path, remote_file_path
            ).is_err()
        );

        upload_mock.assert();
    }

    #[test]
    fn return_error_if_could_not_to_connect() {
        let session = SwiftStorageSession {
            auth_token: get_random_string(),
            storage_url: "http://localhost:11111".to_string(),
            created: 0,
        };

        let client = Client::new();

        let src_file = NamedTempFile::new().unwrap();

        let src_file_path = format!("{}", src_file.path().display());

        assert!(
            upload_file(
                &client, &session, &src_file_path, &get_random_string()
            ).is_err()
        )
    }

    #[test]
    fn return_error_if_source_file_does_not_exist() {
        let session = get_invalid_session(&get_random_string());

        let client = Client::new();

        assert!(
            upload_file(
                &client, &session, &get_random_string(),
                &get_random_string()
            ).is_err()
        )
    }
}
