use httpmock::{Mock, MockServer, Regex};
use httpmock::Method::{DELETE, GET, PUT};
use log::info;
use reqwest::StatusCode;

use crate::{AUTH_ENDPOINT, AUTH_KEY_HEADER, AUTH_TOKEN_HEADER, AUTH_USER_HEADER, STORAGE_URL_HEADER};
use crate::upload::CONTENT_LENGTH_HEADER;

pub const MOCK_AUTH_TOKEN: &str = "whatever-token";

pub fn get_login_mock<'a>(login_server: &'a MockServer, storage_server: &'a MockServer,
                          username: &'a str,
                          password: &'a str) -> Mock<'a> {
    login_server.mock(|when, then| {
        when.method(GET)
            .path(AUTH_ENDPOINT)
            .header(AUTH_USER_HEADER, username)
            .header(AUTH_KEY_HEADER, password);
        then.status(StatusCode::NO_CONTENT.as_u16())
            .header(AUTH_TOKEN_HEADER, MOCK_AUTH_TOKEN)
            .header(STORAGE_URL_HEADER, storage_server.url(""));
    })
}

pub fn get_login_failure_mock<'a>(login_server: &'a MockServer, username: &'a str,
                                  password: &'a str, status: u16) -> Mock<'a> {
    login_server.mock(|when, then| {
        when.method(GET)
            .path(AUTH_ENDPOINT)
            .header(AUTH_USER_HEADER, username)
            .header(AUTH_KEY_HEADER, password);
        then.status(status);
    })
}

/// Returns HTTP CREATED on every PUT request
pub fn upload_file_mock<'a>(mock_server: &'a MockServer) -> Mock<'a> {
    mock_server.mock(|when, then| {
        when.method(PUT)
            .header_exists(AUTH_TOKEN_HEADER)
            .header_exists(CONTENT_LENGTH_HEADER);
        then.status(StatusCode::CREATED.as_u16());
    })
}

/// Returns HTTP CREATED on every PUT request, assert path.
///
/// - `path` expected path
pub fn list_container_files_mock<'a>(mock_server: &'a MockServer, path: &'a str) -> Mock<'a> {
    info!("list_container_files_mock mock:");
    info!("- mock server url '{}'", mock_server.url(""));
    info!("- path '{}'", path);

    let pattern = format!("(^/)?{path}");
    let path_regex = Regex::new(&pattern).unwrap();

    mock_server.mock(|when, then| {
        when.method(GET)
            .header_exists(AUTH_TOKEN_HEADER)
            .path_matches(path_regex);
        then.status(StatusCode::OK.as_u16());
    })
}

/// Returns HTTP CREATED on every PUT request, assert path.
///
/// - `path` expected path
pub fn upload_file_path_mock<'a>(mock_server: &'a MockServer, path: String) -> Mock<'a> {
    info!("upload_file_path_mock mock:");
    info!("- mock server url '{}'", mock_server.url(""));
    info!("- path '{path}'");

    let pattern = format!("(^/)?{path}");
    let path_regex = Regex::new(&pattern).unwrap();

    mock_server.mock(|when, then| {
        when.method(PUT)
            .header_exists(AUTH_TOKEN_HEADER)
            .header_exists(CONTENT_LENGTH_HEADER)
            .path_matches(path_regex);
        then.status(StatusCode::CREATED.as_u16());
    })
}

/// Returns `NO_CONTENT` on success
///
/// - `path` expected path
pub fn delete_file_mock<'a>(mock_server: &'a MockServer, path: &'a str) -> Mock<'a> {
    info!("delete_file_mock mock:");
    info!("- mock server url '{}'", mock_server.url(""));
    info!("- path '{}'", path);

    let pattern = format!("(^/)?{path}");
    let path_regex = Regex::new(&pattern).unwrap();

    mock_server.mock(|when, then| {
        when.method(DELETE)
            .header_exists(AUTH_TOKEN_HEADER)
            .header_exists(CONTENT_LENGTH_HEADER)
            .path_matches(path_regex);
        then.status(StatusCode::NO_CONTENT.as_u16());
    })
}

/// Returns `NO_CONTENT` on success
///
/// - `path` expected path
pub fn delete_files_any_mock(mock_server: &MockServer) -> Mock {
    info!("delete_file_mock mock:");
    info!("- mock server url '{}'", mock_server.url(""));

    mock_server.mock(|when, then| {
        when.method(DELETE)
            .header_exists(AUTH_TOKEN_HEADER)
            .header_exists(CONTENT_LENGTH_HEADER);
        then.status(StatusCode::NO_CONTENT.as_u16());
    })
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use httpmock::MockServer;
    use log::{info, LevelFilter};
    use reqwest::blocking::Client;
    use reqwest::StatusCode;

    use crate::AUTH_TOKEN_HEADER;
    use crate::mock::upload_file_path_mock;
    use crate::upload::CONTENT_LENGTH_HEADER;

    fn init() {
        let _ = env_logger::builder().filter_level(LevelFilter::Debug)
            .is_test(true).try_init();
    }

    #[test]
    fn should_return_created_status() {
        init();

        let server = MockServer::start();

        let path = "some/path";

        let upload_file_mock = upload_file_path_mock(&server, path.to_string());

        let client = Client::new();

        info!("request url: {}", server.url(path));

        let request_url = format!("{}/{}", server.url(""), path);

        match client.put(&request_url)
            .timeout(Duration::new(3, 0))
            .header(AUTH_TOKEN_HEADER, "token")
            .header(CONTENT_LENGTH_HEADER, "4")
            .body("abcd")
            .send() {
            Ok(resp) => {
                let status: reqwest::StatusCode = resp.status();
                assert_eq!(StatusCode::CREATED, status);
                upload_file_mock.assert();
            }
            Err(_) => panic!("result expected")
        }
    }

}
