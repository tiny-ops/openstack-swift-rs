pub fn normalize_url_slashes(url: &str) -> String {
    url.replace("//", "/").replace(":/", "://")
}

#[cfg(test)]
mod normalize_url_slashes_tests {
    use crate::url::normalize_url_slashes;

    #[test]
    fn respect_protocol() {
        let data: Vec<(&str,&str)> = vec![
            ("https://rust-lang.com//a//b//c", "https://rust-lang.com/a/b/c"),
            ("http://rust-lang.com//a//b", "http://rust-lang.com/a/b"),
        ];

        for test_pair in data {
            assert_eq!(test_pair.1, &normalize_url_slashes(test_pair.0))
        }
    }

    #[test]
    fn valid_values_should_be_returned_as_is() {
        let data: Vec<(&str,&str)> = vec![
            ("https://rust-lang.com", "https://rust-lang.com"),
            ("https://rust-lang.com/docs/borrowing", "https://rust-lang.com/docs/borrowing")
        ];

        for test_pair in data {
            assert_eq!(test_pair.1, &normalize_url_slashes(test_pair.0))
        }
    }
}
