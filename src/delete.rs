use log::{debug, error, info};
use reqwest::blocking::Client;
use reqwest::StatusCode;

use crate::AUTH_TOKEN_HEADER;
use crate::error::get_operation_error_by_status_code;
use crate::error::types::OperationError;
use crate::session::SwiftStorageSession;
use crate::url::normalize_url_slashes;

/// # Delete file from remote storage
///
/// Arguments:
///
/// `session` - user session
///
/// `remote_file_path` - full path to remote file
///
pub fn delete_file(client: &Client, session: &SwiftStorageSession,
                   remote_file_path: &str) -> Result<(), OperationError> {
    info!("delete file from path '{}'", remote_file_path);

    let raw_url = format!("{}/{}", session.storage_url, remote_file_path);

    let url = normalize_url_slashes(&raw_url);

    info!("url: '{}'", url);

    match client.delete(&url)
        .header(AUTH_TOKEN_HEADER, &session.auth_token)
        .send() {
        Ok(resp) => {
            debug!("response code: {}", resp.status());

            if resp.status() == StatusCode::NO_CONTENT {
                info!("file '{}' has been removed from remote storage", remote_file_path);
                Ok(())

            } else {
                Err(get_operation_error_by_status_code(resp.status()))
            }
        }
        Err(e) => {
            error!("unable to upload file: {}", e);
            Err(OperationError::ConnectionError(e))
        }
    }
}

/// # Delete files
///
/// Arguments:
///
/// `session` - user session
///
/// `file_paths` - files for delete
///
pub fn delete_files(client: &Client, session: &SwiftStorageSession,
                    file_paths: &Vec<String>) -> Result<(), OperationError> {
    info!("delete files '{:?}'", file_paths);

    let url = format!("{}?bulk-delete=true", session.storage_url);
    info!("url: '{}'", url);

    let body = file_paths.join("\\n");

    debug!("<request body>");
    debug!("{}", body);
    debug!("</request body>");

    match client.post(&url)
        .header(AUTH_TOKEN_HEADER, &session.auth_token)
        .header("Content-Type", "text/plain")
        .body(body)
        .send() {
        Ok(resp) => {
            debug!("response code: {}", resp.status());

            if resp.status() == StatusCode::OK {
                info!("files have been removed from remote storage");
                Ok(())

            } else {
                Err(get_operation_error_by_status_code(resp.status()))
            }
        }
        Err(e) => {
            error!("couldn't delete files: {}", e);
            Err(OperationError::ConnectionError(e))
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use reqwest::blocking::Client;
    use tempfile::NamedTempFile;

    use crate::delete::delete_file;
    use crate::error::types::OperationError;
    use crate::login::login_to_remote_storage;
    use crate::test_utils::{get_connection_profile, get_invalid_session, get_random_string, init_logging};
    use crate::upload::upload_file;

    #[test]
    fn return_ok_after_success_delete() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &connection_profile.password) {
            Ok(session) => {

                let src_file = NamedTempFile::new().unwrap();
                let file_path = format!("{}", src_file.path().display());

                fs::write(&file_path, &get_random_string()).unwrap();

                let target_path = format!("{}/test_ok", &connection_profile.container);

                match upload_file(&client, &session, &file_path, &target_path) {
                    Ok(_) => {

                        match delete_file(&client, &session, &target_path) {
                            Ok(_) => assert!(true),
                            Err(_) => panic!("result expected")
                        }

                    },
                    Err(_) => panic!("result expected")
                }

            },
            Err(_) => panic!("session expected")
        }
    }

    #[test]
    fn return_auth_error_for_invalid_session() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        let session = get_invalid_session(&connection_profile.storage_url);

        match delete_file(&client, &session, &get_random_string()) {
            Ok(_) => panic!("auth error expected"),
            Err(e) => {
                match e {
                    OperationError::Authentication => assert!(true),
                    _ => panic!("auth error expected")
                }
            }
        }
    }
}

#[cfg(test)]
mod delete_files_tests {
    use std::fs;

    use reqwest::blocking::Client;
    use tempfile::NamedTempFile;

    use crate::delete::delete_files;
    use crate::list::list_container_files;
    use crate::login::login_to_remote_storage;
    use crate::test_utils::{get_connection_profile, get_random_string, init_logging};
    use crate::upload::upload_file;

    #[test]
    fn return_ok_after_success_delete() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &connection_profile.password) {
            Ok(session) => {

                let src_file1 = NamedTempFile::new().unwrap();
                let file_path1 = format!("{}", src_file1.path().display());
                fs::write(&file_path1, &get_random_string()).unwrap();
                let target_path1 = format!("{}/test_ok1", &connection_profile.container);

                let src_file2 = NamedTempFile::new().unwrap();
                let file_path2 = format!("{}", src_file2.path().display());
                fs::write(&file_path2, &get_random_string()).unwrap();
                let target_path2 = format!("{}/test_ok2", &connection_profile.container);

                upload_file(&client, &session, &file_path1,
                            &target_path1).unwrap();
                upload_file(&client, &session, &file_path2,
                            &target_path2).unwrap();

                let paths = vec![target_path1, target_path2];

                match delete_files(&client, &session, &paths) {
                    Ok(_) => {
                        let results = list_container_files(
                            &client, &session, &connection_profile.container).unwrap();

                        println!("results: {:?}", results);

                        assert!(!results.contains(&"test_ok1".to_string()));
                        assert!(!results.contains(&"test_ok2".to_string()));
                    },
                    Err(_) => panic!("result expected")
                }

            },
            Err(_) => panic!("session expected")
        }
    }
}
