use chrono::{DateTime, Local};
use log::{debug, error, info};
use reqwest::StatusCode;

use crate::{AUTH_ENDPOINT, AUTH_KEY_HEADER, AUTH_TOKEN_HEADER, AUTH_USER_HEADER, STORAGE_URL_HEADER};
use crate::error::types::OperationError;
use crate::session::SwiftStorageSession;
use crate::url::normalize_url_slashes;

/// Get swift storage session
///
/// Arguments:
///
/// `base_auth_url` - base API auth url
///
/// `username` - username
///
/// `password` - password
///
pub fn login_to_remote_storage(client: &reqwest::blocking::Client,
                               base_auth_url: &str, username: &str,
                               password: &str) -> Result<SwiftStorageSession, OperationError> {
    info!("login to swift storage");
    info!("- base auth url: '{}'", base_auth_url);
    info!("- username: '{}'", username);

    let raw_url = format!("{}{}", base_auth_url, AUTH_ENDPOINT);
    let request_url = normalize_url_slashes(&raw_url);

    debug!("request url: '{}'", request_url);

    let resp = client.get(&request_url)
        .header(AUTH_USER_HEADER, username)
        .header(AUTH_KEY_HEADER, password)
        .send()?;

    debug!("status code: {}", resp.status());

    if resp.status() == StatusCode::NO_CONTENT {
        info!("auth ok");

        if resp.headers().contains_key(AUTH_TOKEN_HEADER) &&
            resp.headers().contains_key(STORAGE_URL_HEADER) {
            let auth_token = resp.headers().get(AUTH_TOKEN_HEADER).unwrap();
            let storage_url = resp.headers().get(STORAGE_URL_HEADER).unwrap();

            let created: DateTime<Local> = Local::now();

            let session = SwiftStorageSession {
                auth_token: String::from(auth_token.to_str()?),
                storage_url: String::from(storage_url.to_str()?),
                created: created.timestamp(),
            };

            info!("session: {}", session);

            Ok(session)

        } else {
            error!("storage server respond, but expected http header(s) are missing:");
            error!("- {}", AUTH_TOKEN_HEADER);
            error!("- {}", STORAGE_URL_HEADER);
            Err(OperationError::UnsupportedApiError)
        }
    } else if resp.status() == StatusCode::UNAUTHORIZED ||
        resp.status() == StatusCode::FORBIDDEN {
        Err(OperationError::Authentication)

    } else {
        error!("unexpected response code from remote storage: {}", resp.status());
        Err(OperationError::UnsupportedApiError)
    }
}

#[cfg(test)]
mod tests {
    use log::error;
    use reqwest::blocking::Client;

    use crate::error::types::OperationError;
    use crate::login::login_to_remote_storage;
    use crate::test_utils::{get_connection_profile, get_random_string, init_logging};

    #[test]
    fn return_session_after_login_succeed() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &connection_profile.password) {
            Ok(session) => {
                assert!(!session.auth_token.is_empty());
                assert!(!session.storage_url.is_empty());
                assert!(session.created > 0);
            },
            Err(_) => panic!("session expected")
        }
    }

    #[test]
    fn return_unsupported_api_error_for_integration_problems() {
        init_logging();

        let client = Client::new();

        match login_to_remote_storage(
            &client, "https://ya.ru",
            &get_random_string(), &get_random_string()
        ) {
            Ok(_) => panic!("UnsupportedApiError expected"),
            Err(e) => {
                match e {
                    OperationError::UnsupportedApiError => assert!(true),
                    _ => {
                        error!("{:?}", e);
                        panic!("UnsupportedApiError expected")
                    }
                }
            }
        }
    }

    #[test]
    fn return_connection_error_for_connectivity_issues() {
        init_logging();

        let client = Client::new();

        match login_to_remote_storage(
            &client, &get_random_string(),
            &get_random_string(), &get_random_string()
        ) {
            Ok(_) => panic!("default error expected"),
            Err(e) => {
                match e {
                    OperationError::ConnectionError(_) => assert!(true),
                    _ => panic!("default error expected")
                }
            }
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_username() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &get_random_string(), &get_random_string()
        ) {
            Ok(_) => panic!("auth error expected"),
            Err(e) => {
                match e {
                    OperationError::Authentication => assert!(true),
                    _ => panic!("auth error expected")
                }
            }
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_password() {
        init_logging();

        let client = Client::new();

        let connection_profile = get_connection_profile();

        match login_to_remote_storage(
            &client, &connection_profile.auth_url,
            &connection_profile.username, &get_random_string()
        ) {
            Ok(_) => panic!("auth error expected"),
            Err(e) => {
                match e {
                    OperationError::Authentication => assert!(true),
                    _ => panic!("auth error expected")
                }
            }
        }
    }
}


