use chrono::{DateTime, Local};
use fake::{Fake, Faker};
use log::LevelFilter;

use crate::session::SwiftStorageSession;

pub fn init_logging() {
    let _ = env_logger::builder()
        .filter_level(LevelFilter::Debug)
        .is_test(true).try_init();
}

pub fn get_invalid_session(storage_url: &str) -> SwiftStorageSession {
    let now: DateTime<Local> = Local::now();

    SwiftStorageSession {
        auth_token: "invalid-token".to_string(),
        storage_url: storage_url.to_string(),
        created: now.timestamp(),
    }
}

pub struct SwiftConnectionProfile {
    pub auth_url: String,
    pub storage_url: String,
    pub username: String,
    pub password: String,
    pub container: String
}

/// Return connection profile (`SwiftConnectionProfile`) for integration tests
pub fn get_connection_profile() -> SwiftConnectionProfile {
    let auth_url = env!("SWIFT_AUTH_URL");
    let storage_url = env!("SWIFT_STORAGE_URL");
    let username = env!("SWIFT_STORAGE_USER");
    let password = env!("SWIFT_STORAGE_PASSWORD");
    let container = env!("SWIFT_STORAGE_CONTAINER");

    SwiftConnectionProfile {
        auth_url: auth_url.to_string(),
        storage_url: storage_url.to_string(),
        username: username.to_string(),
        password: password.to_string(),
        container: container.to_string()
    }
}

pub fn get_random_string() -> String {
    Faker.fake::<String>()
}
